<?php

/**
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @copyright Metaways Infosystems GmbH, 2012
 * @copyright Aimeos (aimeos.org), 2014-2017
 */

/* Available data:
 * - productItem : Product item incl. referenced items
 * - params : Request parameters for this detail view
 * - mediaItems : Media items incl. referenced items
 */

$enc = $this->encoder();

$getVariantData = function( \Aimeos\MShop\Media\Item\Iface $mediaItem ) use ( $enc )
{
	$string = '';

	foreach( $mediaItem->getRefItems( 'attribute', null, 'variant' ) as $id => $item ) {
		$string .= ' data-variant-' . $item->getType() . '="' . $enc->attr( $id ) . '"';
	}

	return $string;
};


$detailTarget = $this->config( 'client/html/catalog/detail/url/target' );
$detailController = $this->config( 'client/html/catalog/detail/url/controller', 'catalog' );
$detailAction = $this->config( 'client/html/catalog/detail/url/action', 'detail' );
$detailConfig = $this->config( 'client/html/catalog/detail/url/config', [] );

$url = $enc->attr( $this->url( $detailTarget, $detailController, $detailAction, $this->get( 'params', [] ), [], $detailConfig ) );
$mediaItems = $this->get( 'mediaItems', [] );


?>
<link rel="stylesheet" href="<?= url('css/smoothproducts.css')?>">
<div class="catalog-detail-image mt-2">
<!--<div class="container">-->
<!--    <div class="row">-->

        <div class="col-md-12">
            <div class="sp-loading"><img src="<?= url('/images/sp-loading.gif')?>" alt=""><br>LOADING IMAGES</div>
            <div class="sp-wrap">
                <?php foreach( $mediaItems as $id => $mediaItem ) : ?>
                    <!-- Excluding LAST PICTURE Object OF EACH PRODUCT -->
                   <?php if($mediaItem !== end($mediaItems)): ?>
                       <?php $mediaUrl = $enc->attr( $this->content( $mediaItem->getUrl() ) );?>
                       <a href="<?= $enc->attr( $mediaUrl ); ?>" itemprop="contentUrl"><img src="<?= $enc->attr( $mediaUrl ); ?>" alt=""></a>
                   <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>

<!--    </div>-->
<!--</div>-->
</div>