<?php

/**
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @copyright Aimeos (aimeos.org), 2015-2017
 */

$enc = $this->encoder();
$level = $this->get( 'level', 0 );
$path = $this->get( 'path', [] );
$params = $this->get( 'params', [] );

$target = $this->config( 'client/html/catalog/lists/url/target' );
$controller = $this->config( 'client/html/catalog/lists/url/controller', 'catalog' );
$action = $this->config( 'client/html/catalog/lists/url/action', 'list' );
$config = $this->config( 'client/html/catalog/lists/url/config', [] );

/** client/html/common/partials/media
 * Relative path to the media partial template file
 *
 * Partials are templates which are reused in other templates and generate
 * reoccuring blocks filled with data from the assigned values. The media
 * partial creates an HTML block of for images, video, audio or other documents.
 *
 * The partial template files are usually stored in the templates/partials/ folder
 * of the core or the extensions. The configured path to the partial file must
 * be relative to the templates/ folder, e.g. "common/partials/media-standard.php".
 *
 * @param string Relative path to the template file
 * @since 2015.08
 * @category Developer
 */


?>
<div class="container-fluid">

    <div class="catSlider border-secondary pt-3 mt-4 bt-4">

    <!-- external heading   -->
    <span class=" shop-cat d-flex justify-content-center text-secondary ">
        <h6 class="mb-3 "><?= $enc->html( $this->translate( 'client', 'SHOP BY CATEGORY' ), $enc::TRUST ); ?></h6>
    </span>

    <div id="categoryIndicators" class="carousel slide" data-ride="carousel" data-interval="13000">

    <?php
    $fItem=1;
    $cat_images = [
        [
            'title'=>'Shawls',
            'image' => 'brown_fron',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
        ],
        [
            'title'=>'Hijabs',
            'image' => 'beach_scarf_feather1',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
        ],
        [
            'title'=>'Women Sweaters',
            'image' => 'black',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
        ],
        [
            'title'=>'Cachmere Scarves',
            'image' => 'cashmere_scarf_bundle',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
        ],

    ];


    $nodes= $this->get( 'nodes');

    /* Using MultipleIterator for parallel iteration for images and nodes
     * Thanks to stackoverflow */
    $iterator = new MultipleIterator;
    $iterator->attachIterator(new ArrayIterator($cat_images));
    $iterator->attachIterator(new ArrayIterator($nodes));

    ?>



        <ol class="carousel-indicators mb-0">

            <li data-target="#categoryIndicators" data-slide-to="0" class="active" ></li>
                <?php for( $i=1; $i<count($cat_images); $i++ ):?>
                    <li data-target="#categoryIndicators" data-slide-to=<?= $i;?> ></li>
                <?php endfor; ?>

        </ol>

        <div class="carousel-inner row w-100 mx-auto " >

            <?php foreach( $iterator as $values ) : ?>

                <?php if( $values[1]->getStatus() > 0 ) : ?>
                     <?php
                         $id = $values[1]->getId(); $config = $values[1]->getConfig();
                         $params['f_name'] = $values[1]->getName( 'url' ); $params['f_catid'] = $id;
                         $class = ( $values[1]->hasChildren() ? ' withchild' : ' nochild' ) . ( isset( $path[$id] ) ? ' active' : '' );
                         $class .= ' catcode-' . $values[1]->getCode() . ( isset( $config['css-class'] ) ? ' ' . $config['css-class'] : '' );
                     ?>

                    <?php if ($fItem==1) :?>
                      <div class="carousel-item col-md-4 active">
                    <?php else : ?>
                      <div class="carousel-item col-md-4">
                    <?php endif; ?>

                    <?php $fItem=0 ?>

                    <a  href="<?= $enc->attr( $this->url( ( $values[1]->getTarget() ?: $target ), $controller, $action, $params, [], $config ) ); ?>">
                        <img class="img-fluid mx-auto d-block img-thumbnail" src="<?= url('/images/' . $values[0]['image'] . '.jpg')?>" alt="<?= $values[0]['title'] ?>">

                        <div class="carousel-caption d-none d-md-block">
                            <h6><?= $values[0]['title'] ?></h6>
                        </div>

                    </a>
                        </div> <!-- carousel-item-->
                  <?php endif; ?>
               <?php endforeach; ?>
           </div> <!-- carousel-inner -->

        <a class="carousel-control-prev" href="#categoryIndicators" role="button" data-slide="prev">
            <i class="fa fa-angle-left text-muted"></i>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#categoryIndicators" role="button" data-slide="next">
            <i class="fa fa-angle-right text-muted"></i>
            <span class="sr-only">Next</span>
        </a>
    </div>  <!-- categoryIndicator -->
    </div> <!-- catslider -->

</div> <!-- fluid -->