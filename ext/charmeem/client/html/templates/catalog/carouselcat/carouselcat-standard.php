<?php

/**
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @copyright Aimeos (aimeos.org), 2015-2017
 */

$enc = $this->encoder();
$level = $this->get( 'level', 0 );
$path = $this->get( 'path', [] );
$params = $this->get( 'params', [] );

$target = $this->config( 'client/html/catalog/lists/url/target' );
$controller = $this->config( 'client/html/catalog/lists/url/controller', 'catalog' );
$action = $this->config( 'client/html/catalog/lists/url/action', 'list' );
$config = $this->config( 'client/html/catalog/lists/url/config', [] );

/** client/html/common/partials/media
 * Relative path to the media partial template file
 *
 * Partials are templates which are reused in other templates and generate
 * reoccuring blocks filled with data from the assigned values. The media
 * partial creates an HTML block of for images, video, audio or other documents.
 *
 * The partial template files are usually stored in the templates/partials/ folder
 * of the core or the extensions. The configured path to the partial file must
 * be relative to the templates/ folder, e.g. "common/partials/media-standard.php".
 *
 * @param string Relative path to the template file
 * @since 2015.08
 * @category Developer
 */


?>



<?php
/**
 * Lower carousel - Categories carousel - Categories carousel - Categories carousel
 * 3-images per slide ** Slides one image at a time
 */
?>

<?php foreach( $this->get( 'nodes', [] ) as $item ) : ?>
    <?php if( count( $item->getChildren() ) > 0 ) : ?>
        <?php $values = array( 'nodes' => $item->getChildren(), 'path' => $path, 'params' => $params, 'level' => $level + 1 ); ?>
        <?= $this->partial( $this->config( 'client/html/catalog/filter/partials/tree', 'catalog/carouselcat/carouselcat-slider-standard.php' ), $values ); ?>
    <?php endif; ?>
<?php endforeach ?>
