<?php

/**
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @copyright Aimeos (aimeos.org), 2016-2017
 */

/* Expected data:
 * - products : List of product items
 * - productItems : List of product variants incl. referenced items (optional)
 * - basket-add : True to display "add to basket" button, false if not (optional)
 * - require-stock : True if the stock level should be displayed (optional)
 * - itemprop : Schema.org property for the product items (optional)
 * - position : Position is product list to start from (optional)
 */

$enc = $this->encoder();
$position = $this->get( 'position' );
$productItems = $this->get( 'productItems', [] );

if( $this->get( 'basket-add', false ) )
{
    $basketTarget = $this->config( 'client/html/basket/standard/url/target' );
    $basketController = $this->config( 'client/html/basket/standard/url/controller', 'basket' );
    $basketAction = $this->config( 'client/html/basket/standard/url/action', 'index' );
    $basketConfig = $this->config( 'client/html/basket/standard/url/config', [] );
    $basketSite = $this->config( 'client/html/basket/standard/url/site' );

    $basketParams = ( $basketSite ? ['site' => $basketSite] : [] );
}


/** client/html/catalog/detail/url/target
 * Destination of the URL where the controller specified in the URL is known
 *
 * The destination can be a page ID like in a content management system or the
 * module of a software development framework. This "target" must contain or know
 * the controller that should be called by the generated URL.
 *
 * @param string Destination of the URL
 * @since 2014.03
 * @category Developer
 * @see client/html/catalog/detail/url/controller
 * @see client/html/catalog/detail/url/action
 * @see client/html/catalog/detail/url/config
 */
$detailTarget = $this->config( 'client/html/catalog/detail/url/target' );

/** client/html/catalog/detail/url/controller
 * Name of the controller whose action should be called
 *
 * In Model-View-Controller (MVC) applications, the controller contains the methods
 * that create parts of the output displayed in the generated HTML page. Controller
 * names are usually alpha-numeric.
 *
 * @param string Name of the controller
 * @since 2014.03
 * @category Developer
 * @see client/html/catalog/detail/url/target
 * @see client/html/catalog/detail/url/action
 * @see client/html/catalog/detail/url/config
 */
$detailController = $this->config( 'client/html/catalog/detail/url/controller', 'catalog' );

/** client/html/catalog/detail/url/action
 * Name of the action that should create the output
 *
 * In Model-View-Controller (MVC) applications, actions are the methods of a
 * controller that create parts of the output displayed in the generated HTML page.
 * Action names are usually alpha-numeric.
 *
 * @param string Name of the action
 * @since 2014.03
 * @category Developer
 * @see client/html/catalog/detail/url/target
 * @see client/html/catalog/detail/url/controller
 * @see client/html/catalog/detail/url/config
 */
$detailAction = $this->config( 'client/html/catalog/detail/url/action', 'detail' );

/** client/html/catalog/detail/url/config
 * Associative list of configuration options used for generating the URL
 *
 * You can specify additional options as key/value pairs used when generating
 * the URLs, like
 *
 *  client/html/<clientname>/url/config = array( 'absoluteUri' => true )
 *
 * The available key/value pairs depend on the application that embeds the e-commerce
 * framework. This is because the infrastructure of the application is used for
 * generating the URLs. The full list of available config options is referenced
 * in the "see also" section of this page.
 *
 * @param string Associative list of configuration options
 * @since 2014.03
 * @category Developer
 * @see client/html/catalog/detail/url/target
 * @see client/html/catalog/detail/url/controller
 * @see client/html/catalog/detail/url/action
 * @see client/html/url/config
 */
$detailConfig = $this->config( 'client/html/catalog/detail/url/config', [] );
?>


<!--  Bootstrap4 Top Carousal Top Carousel Top  Carousel Top  Carousel -->
<div class="container-fluid">

    <div id="app"><!-- Implementing Vue plugin vue-mq, for media query -->
      <div v-cloak>
        <div id="topIndicators" class="carousel slide " data-ride="carousel">

            <!-- initiating first li item to be true -->
            <?php $fImage=true; $fItem=true; ?>

            <!-- BS Carousel indicators -->
            <ol class="carousel-indicators">
                <!-- creating only 5 sliding elements -->
                <?php for( $i=0; $i<5; $i++ ):?>
                    <?php if ( $fImage === true ): $fImage = false; ?>
                        <li data-target="#topIndicators" data-slide-to=<?= $i;?> class="active" ></li>
                    <?php else: ?>
                        <li data-target="#topIndicators" data-slide-to=<?= $i;?> ></li>
                    <?php endif; ?>
                <?php endfor; ?>
            </ol>

            <!-- BS Carousel elements -->
            <div class="carousel-inner">

                <!-- initializing slider variable -->
                <?php $s=0;?>

                <!-- First iteration for fetching product item-->
                <?php foreach( $this->get( 'products', [] ) as $id => $productItem ) : ?>

                    <!-- fetching parameters for a- link below -->
                    <?php $params = array( 'd_name' => $productItem->getName( 'url' ), 'd_prodid' => $id ); ?>

                   <!-- Fetching image preview -->
                    <?php
                        $mediaItems = $productItem->getRefItems( 'media', 'default', 'default' );

                        //FETCHING FIRST PICTURE which is Full size
                        $mediaItemf = reset($mediaItems);

                        //FETCHING LAST PICTURE Object OF EACH PRODUCT which is photoshop reduced height
                        $mediaIteml = end($mediaItems);

                        $mediaUrlf = $enc->attr( $this->content( $mediaItemf->getPreview() ) );
                        $mediaUrll = $enc->attr( $this->content( $mediaIteml->getPreview() ) );
                    ?>

                    <!-- active class only for starting image -->
                    <?php if ($fItem === true): $fItem = false; ?>
                        <div class="carousel-item active">
                    <?php else: ?>
                        <div class="carousel-item">
                    <?php endif; ?>

                        <!-- encapsulating image withing a-link fr detail product page-->
                        <a href="<?= $enc->attr( $this->url( ( $productItem->getTarget() ?: $detailTarget ), $detailController, $detailAction, $params, [], $detailConfig ) ); ?>">

                            <!-- Implementing Vue plugin vue-mq, for media query -->
                                <div v-if="$mq ==='mobile'">
                                    <img class="d-block w-100" src=" <?= url($mediaUrlf) ?> " alt="<?= $enc->attr( $mediaItemf->getName() ) ?>">
                                </div>
                                <div v-else>
                                    <img class="d-block w-100" src=" <?= url($mediaUrll) ?> " alt="<?= $enc->attr( $mediaIteml->getName() ) ?>">
                                </div>


                            <!-- Disabling caption -->
                            <?php /*
                            <div class="carousel-caption d-none d-md-block">
                                <h2  itemprop="name"><?= $enc->html( $productItem->getName(), $enc::TRUST ); ?></h2>
                            </div> <!--carousel-caption -->
                            */?>
                        </a>

                        </div> <!-- carousel-item-->

                    <!-- limiting only to 5 images per slider -->
                    <?php $s++; if($s===5) break; ?>

                <?php endforeach; ?>   <!--outer endforeach -->

            </div>  <!-- carousel-inner -->

                <a class="carousel-control-prev" href="#topIndicators" role="button" data-slide="prev">
                    <i class="fa fa-angle-left text-muted"></i>
                    <span class="sr-only">Previous</span>
                </a>

                <a class="carousel-control-next" href="#topIndicators" role="button" data-slide="next">
                    <i class="fa fa-angle-right text-muted"></i>
                    <span class="sr-only">Next</span>
                </a>

         </div> <!-- topIndicator -->
        </div> <!--v-cloak-->
      </div>  <!-- vue div -->
    </div> <!--container_fluid -->


<!--<div id="app">-->
<!--    <h1> {{displayText}} </h1>-->
<!--    <h2>You are using: {{$mq}}</h2>-->
<!--   <grid-component></grid-component>-->
<!--</div>-->

