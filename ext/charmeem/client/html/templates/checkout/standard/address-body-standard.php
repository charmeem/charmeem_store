<?php

/**
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @copyright Metaways Infosystems GmbH, 2013
 * @copyright Aimeos (aimeos.org), 2015-2017
 */

$enc = $this->encoder();

?>
<?php $this->block()->start( 'checkout/standard/address' ); ?>

<section class="checkout-standard-address mx-4">

	<h1><?= $enc->html( $this->translate( 'client', 'address' ), $enc::TRUST ); ?></h1>
	<p class="note">
		<small><?= $enc->html( $this->translate( 'client', 'Fields with * are mandatory' ), $enc::TRUST ); ?></small>
	</p>


	<div class="form-horizontal row">
		<?= $this->block()->get( 'checkout/standard/address/billing' ); ?>

		<?= $this->block()->get( 'checkout/standard/address/delivery' ); ?>
	</div>


	<div class="button-group pt-0 mb-4">
		<a class="btn btn-default  btn-back" href="<?= $enc->attr( $this->get( 'standardUrlBack' ) ); ?>">
			<?= $enc->html( $this->translate( 'client', 'Previous' ), $enc::TRUST ); ?>
		</a>
		<button class="btn btn-next btn-action">
			<?= $enc->html( $this->translate( 'client', 'Next' ), $enc::TRUST ); ?>
		</button>
	</div>

</section>
<?php $this->block()->stop(); ?>
<?= $this->block()->get( 'checkout/standard/address' ); ?>
