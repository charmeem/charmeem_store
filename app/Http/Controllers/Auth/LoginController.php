<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
//use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /** This is a trait */
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     * I am redirecting to checkout in all the cases????
     *
     * @var string
     */
 protected $redirectTo = '/style/list';



    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }







    /**
     * Customization for Redirection to the previous page
     * after login is done
     * charmeem
     */

//    protected function redirectTo()
//    {
//        if ($request->has('previous')) {
//            $redirectTo = $request->get('previous');
//        }
//
//        return $redirectTo = '/home';
//    }

//    public function authenticated(Request $request, $user)
//    {
//        $request->session()->regenerate();
//
////        $this->clearLoginAttempts($request);
//
//
//        return redirect()->intended($this->redirectPath());
//        $topi = redirect()->intended();
//        var_dump($topi);
//        var_dump($this->redirectPath());

//        return redirect()->back();

//    if ($request->has('previous')) {
//        $this->redirectTo = $request->get('previous');
//    }
////return " This is loginController";
//    return $this->redirectTo = '/style/list';

//    }


}
