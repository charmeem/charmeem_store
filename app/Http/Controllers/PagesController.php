<?php

namespace App\Http\Controllers;

use App\Mail\sendMailable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class PagesController extends Controller
{
   /*
    |--------------------------------------------------------------------------
    | Pages Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the Email sending from
    | Contact Page form
    |
    */

    public function __construct()
    {
//        $this->middleware('auth');
    }

    public function getContact(){
        return view ('pages.contact');
    }

    public function postContact(Request $request) {

        $this->validate($request, [
           'email'   => 'required|email',
           'subject' => 'min:3',
           'message' => 'min:10'
        ]);
        $contact=[];
       $contact['email'] = $request->get('email');
       $contact['subject'] = $request->get('subject');
       $contact['messageBody'] = $request->get('message');

       Mail::to('mmufti@hotmail.com')
//           ->cc('66mufti@gmail.com')
           ->send(new sendMailable($contact));

        session()->flash('contact', 'Your E-mail has been successfully sent!');

        return Redirect::back();
//        return redirect('/');
    }
}
