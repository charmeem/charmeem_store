<?php

namespace App\Http\Controllers;

use App\Mail\registerMailable;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class ContactregisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    /** This is a trait */
    use RegistersUsers;
//    protected $redirectTo = '/checkoutlogin';

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
//    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Overriding parent's trait
     *
     * Show the application registration form, linked from route file.
     *
     */
    public function showRegistrationForm()
    {
        return view('auth.registercontact');
    }


    /**
     * Override default register method from RegistersUsers trait
     *
     * @param array $request
     * @return redirect to $redirectTo
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        // Fetch activation_key from getToken and add it to the request object
        $activation_key = $this->getToken();
//        var_dump($activation_key);
        $request->request->add(['activation_key'=> $activation_key]);

        // Store all $request object received from reg. form plus activ. key in the database
        event(new Registered($user = $this->create($request->all())));

        //        $this->guard()->login($user);

        // Send mail with the activation link , will use sendMailable
        $contact = ['name' => $request['name'],'email'=> $request['email'], 'activation_link' => url('/activation_contact/' . $activation_key) ];

        Mail::to($contact['email'])
            ->send(new registerMailable($contact));

//        return $this->registered($request, $user)
//            ?: redirect($this->redirectPath())->with('success', 'We have sent an activation link on your email id. Please activate and login.');
//
        return('We have sent an activation link on your email id. Please activate and login.');
    }

    /**
     * Generate a unique token
     *
     * @return unique token
     */
    public function getToken() {
        return hash_hmac('sha256', str_random(40), config('app.key'));
    }

    /**
     * Activate the user account
     *
     * @param string $key
     * @return redirect to login page
     */
    public function activation($key)
    {
        $auth_user = User::where('activation_key', $key)->first();

        if($auth_user) {
            $auth_user->status_a = 'active';
            $auth_user->save();
            return redirect('contactlog')->with('success', 'Your account is activated. You can login now.');
        } else {
            return redirect('contactlog')->with('error', 'Invalid activation key.');
        }
    }

    /**
     * Override default registered method from RegistersUsers trait
     *
     * The user has been registered. and redirecting to delivery page
     *
     */
    protected function registered()
    {
//        return redirect()->route('aimeos_shop_checkout', ['site' => 'style', 'c_step?' =>'delivery']);
        return redirect('contact');
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'activation_key' => $data['activation_key'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
