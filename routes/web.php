<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    return view('welcome');

    return redirect('style/list');
});

/* Registeration activation Route for Checkout Page*/
Route::get('/activation/{key}', 'checkoutregController@activation');

/* Registeration activation Route for Contact Page*/
Route::get('/activation_contact/{key}', 'ContactregisterController@activation');

/* Routes to pages linked from Navbar */
Route::get('/story', function() {
    return view('pages.story');
});

Route::get('/faq', function() {
    return view('pages.faq');
});

//Route::get('/contact', function() {
//    return view('pages.contact');
//});

/** Route for accessing and sending form of Contact Page Login*/
Route::match( array( 'GET' ),'/contactlog' , array(
    'as' => 'aimeos_shop_contactlog',
    'uses' => 'ContactloginController@showLoginForm',
));

Route::match( array( 'POST' ),'/contactlog' , array(
    'as' => 'aimeos_shop_contactlog',
    'uses' => 'ContactloginController@login',  // will go to parent trait of checkoutlogincontroller
));

Route::get('/contact', 'PagesController@getContact'); //page rendering

Route::post('/contact', 'PagesController@postContact'); //form submission

/** Route for accesing and sending form of Contact Page Registration*/
Route::match( array( 'GET' ),'/contactregister' , array(
    'as' => 'aimeos_shop_contactreg',
    'uses' => 'ContactregisterController@showRegistrationForm',
));

Route::match( array( 'POST' ),'/contactregister' , array(
    'as' => 'aimeos_shop_contactreg',
    'uses' => 'ContactregisterController@register',
));


Route::get('/return', function() {
    return view('pages.return');
});

Route::get('/shipping', function() {
    return view('pages.shipping');
});

Route::get('/sale', function() {
    return view('pages.sale');
});

/** Route for login during checkout process
 *  A new controller and method is created for this spurpose
 */
Route::match( array( 'GET' ),'/checkoutlogin' , array(
    'as' => 'aimeos_shop_login',
    'uses' => 'checkoutloginController@showLoginForm',
));

Route::match( array( 'POST' ),'/checkoutlogin' , array(
    'as' => 'aimeos_shop_login',
    'uses' => 'checkoutloginController@login',  // will go to parent trait of checkoutlogincontroller
));

/** Route for register during checkout process
 *  A new controller and method is created for this spurpose
 */
Route::match( array( 'GET' ),'/checkoutregister' , array(
    'as' => 'aimeos_shop_register',
    'uses' => 'checkoutregController@showRegistrationForm',
));

Route::match( array( 'POST' ),'/checkoutregister' , array(
    'as' => 'aimeos_shop_register',
    'uses' => 'checkoutregController@register',  // will go to parent trait of checkoutregcontroller
));



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
