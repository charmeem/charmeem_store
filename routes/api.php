<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
     return $request->user();
});

//Route::middleware('auth:api')->get('/admin/{site}/jsonadm/{resource}/{id}', function(){
//    return redirect()->route('aimeos_shop_jsonadm_get');
//});

Route::get('/gopi', function(){
//    return redirect()->route('aimeos_shop_jsonadm_options',['site'=>'style','resource'=>'order']);
    return redirect()->route('aimeos_shop_jsonapi_get',['resource'=>'order']);


});