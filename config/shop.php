<?php

return [

    'routes' => [
        // 'login' => ['middleware' => ['web']],
        // 'admin' => ['prefix' => 'admin', 'middleware' => ['web']],
        // 'jqadm' => ['prefix' => 'admin/{site}/jqadm', 'middleware' => ['web', 'auth']],
        // 'extadm' => ['prefix' => 'admin/{site}/extadm', 'middleware' => ['web', 'auth']],
         'jsonadm' => ['prefix' => 'admin/{site}/jsonadm', 'middleware' => ['web', 'auth:api']],
//       'jsonapi' => ['prefix' => 'jsonapi', 'middleware' => ['web', 'auth:api']],
        'account' => ['prefix' => '{site}', 'middleware' => ['web', 'auth']],
        'default' => ['prefix' => '{site}', 'middleware' => ['web']],
        'confirm' => ['prefix' => '{site}', 'middleware' => ['web']],
        // 'update' => [],
    ],

    'page' => [
        // 'account-index' => [ 'account/profile','account/subscription','account/history','account/favorite','account/watch','basket/mini','catalog/session' ],
        // 'basket-index' => [ 'basket/standard','basket/related' ],
        // 'catalog-count' => [ 'catalog/count' ],
        'catalog-detail' => [ 'basket/mini','catalog/filter','catalog/stage','catalog/detail','catalog/session' ],
        // Hint: catalog/filter is also available as single 'catalog/tree', 'catalog/search', 'catalog/attribute' (https://aimeos.org/docs/Laravel/Adapt_pages)
        'catalog-list' => [ 'basket/mini','catalog/filter','catalog/stage','catalog/lists', 'catalog/tree', 'catalog/search', 'catalog/attribute', 'catalog/carouselcat', 'catalog/carouseltop' ],
        // 'catalog-stock' => [ 'catalog/stock' ],
        // 'catalog-suggest' => [ 'catalog/suggest' ],
        // 'checkout-confirm' => [ 'checkout/confirm' ],
        // 'checkout-index' => [ 'checkout/standard' ],
        // 'checkout-update' => [ 'checkout/update' ],
    ],

    /*
    'resource' => [
        'db' => [
            'adapter' => env('DB_CONNECTION', 'mysql'),
            'host' => env('DB_HOST', 'localhost'),
            'port' => env('DB_PORT', ''),
            'socket' => '',
            'database' => env('DB_DATABASE', 'laravel'),
            'username' => env('DB_USERNAME', 'root'),
            'password' => env('DB_PASSWORD', ''),
            'stmt' => ["SET SESSION sort_buffer_size=2097144; SET NAMES 'utf8'; SET SESSION sql_mode='ANSI'"],
            'limit' => 3, // maximum number of concurrent database connections
        ],
    ],
    */

    'admin' => [],

    'client' => [
        'html' => [
            'basket' => [
                'cache' => [
                    'enable' => false, // Disable basket content caching
                ],
            ],
            'catalog' => [
                'carouselcat' => [
                    'name' => 'standard'
                ],
                'carouseltop' => [
                    'name' => 'standard'
                ],
            ],
            'checkout' => [
                'standard' => [
                    'url' => [
                        'targetlogin' => 'aimeos_shop_login',
                    ],
                ],
            ],
            'common' => [
                'content' => [
//					 'baseurl' => config( 'app.url' ) . '/',
                    'baseurl' => 'http://charmeem',
//                    'baseurl' => 'https://charmeem.com',
//                    'baseurl' =>'http://charmeem.us-east-2.elasticbeanstalk.com',
                ],
                'template' => [
                    'baseurl' => public_path( 'packages/aimeos/shop/themes/elegance' ),
                ],
            ],
            'email' => [
                'from-email' => 'orders@charmeem.com',
                'from-name'  => 'Your Order at Charmeem Mall',
            ],

        ],
    ],

    'controller' => [
    ],

    'i18n' => [
    ],

    'madmin' => [
        'cache' => [
            'manager' => [
                'name' => 'None', // Disable caching
            ],
        ],
        'log' => [
            'manager' => [
                'standard' => [
                    // 'loglevel' => 7, // Enable debug logging into madmin_log table
                ],
            ],
        ],
    ],

    'mshop' => [
    ],


    'command' => [
    ],

    'frontend' => [
    ],

    'backend' => [
    ],

];
