/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


/* Duplicate- Causing problem in navbar collapse-Bootstrap already included in _script.blade */
//require('./bootstrap');

window.Vue = require('vue');

/* Importing and using vue-mq plugin for responsiveness */
// import Vue from 'vue';
import VueMq from 'vue-mq';

Vue.use(VueMq, {
    breakpoints: {
        mobile: 450,
        bmobile: 528,
        tablet: 900,
        laptop: 1250,
        desktop: Infinity,
    }
});

const app = new Vue({

    el: '#app'
    // data:{
    //     $mq
    // }
    // computed: {
    // //     displayText() {
    //         return this.$mq === 'mobile' ? 'I am small' : 'I am large'
    //     },
    //     mobile() {
    //         return this.$mq === 'mobile' ? true :false
    //     }
    // }
})
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('responsive-component', require('./components/ResponsiveComponent.vue'));
// Vue.component('example-component', require('./components/ExampleComponent.vue'));
// Vue.component('grid-component', {
//
//     template:`<div :column="$mq | mq({ sm: 1, md: 2, lg: 3 })"></div>`
//
// });
