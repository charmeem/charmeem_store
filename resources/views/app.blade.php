<?php
//if(DB::connection()->getDatabaseName())
//{
//    echo "Connected to database ".DB::connection()->getDatabaseName();
//}
////$response->send();
//?>

<!DOCTYPE html>
<html lang="en" class="no-js">

    @include('partials._head')

    <body>

        <div class="fixed-top">
            @include('partials._topbar')
        </div>

        @include('partials._navbar')

        <div class="container">
            @yield('contact')
            @yield('faq')
            @yield('story')
            @yield('return')
        </div>

        <!-- Signin functionality on Home page-->
        <!-- Decided to remove it ,see lazy_laravel for detail -->
           <!-- Adding Sign In Modal on topbar-->
        {{--<div id="signinMod" class="modal fade">--}}
            {{--<div class="modal-dialog modal-sm">--}}
                {{--<div class="modal-content">--}}
                    {{--<div class="modal-body">--}}
                        {{--<button type="button" class="btn btn-warning">--}}
                            {{--<a href="{{route('aimeos_shop_login')}}" >Sign In</a>--}}
                        {{--</button>--}}
                        {{--or New Customer? <a href="{{route('aimeos_shop_register')}}">clickhere</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        <!-- Carousals plus catalog lists
             New Components for carousels are created, see lazy_laravel
         -->
        @yield('aimeos_body')
        <!--@yield('aimeos_aside')
        @yield('content') -->

       @include('partials._footer')

       @include('partials._scripts')


    </body>
</html>