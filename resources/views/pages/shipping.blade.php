@extends('app')
@section('title','| Shipping')
@section('return')
    <div class="row">
        <div class="col-md-12">
            <div class="jumbotron">
                <h3> Shipping Policy </h3>
                <hr>
                <br>
                <h5>Local shipping within Pakistan</h5>
                <p class="lead">
                    We cover main cities of Pakistan with following rates:
                    200RS upto 1kg
                    + 100RS for every additional KG
               </p>
                <h5>International Shipping</h5>
                <p class="lead">We Ship globaly although international shipping option is currently not activated in checkout form.
                You can contact us below to get the international shipping cost for your country.

                </p>
                <hr class="my-4">

            </div>
        </div>
    </div>
@endsection