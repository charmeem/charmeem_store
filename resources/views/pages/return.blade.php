@extends('app')
@section('title','| Return')
@section('return')
    <div class="row">
        <div class="col-md-12">
            <div class="jumbotron">
                <h3> Return Policy </h3>
                <br>
                <h5>Refund Guarantee</h5>
                <ul>
                    <li>
                        We will refund you the money if
                    </li>
                    <li class="ml-3">
                        You did not receive Your Order, or
                    </li>
                    <li class="ml-3">
                        The Item is demaged, or
                    </li>
                    <li class="ml-3">
                        Item is significantly different as its described
                    </li>

                </ul>
<p>However You have to return the item within 3 days.</p>
                <hr class="my-4">

            </div>
        </div>
    </div>
@endsection