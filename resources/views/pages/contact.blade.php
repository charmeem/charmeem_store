@extends('app')

@section('title','| Contact')

@section('contact')
    <div class="container">
        @if(session()->has('contact'))
            <div class="row">
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p>{{session()->get('contact')}}</p>
                </div>
            </div>
        @endif

    </div>

    <div id="app"></div>
    <div class="row">
        <div class="col-md-12">
            {{--<div class="jumbotron">--}}
                <h1 class="display-4">Contact Us</h1>
                <hr>
                {{--<p>Your suggestion, queries are welcome :</p>--}}
            {{--</div>--}}

            <form action="{{Auth::check() ? url('/contact') : url('/contactlog')}}"
                  method={{Auth::check() ? "POST" : "GET"}}>
                {{csrf_field()}}
                <div class="col-4">
                <div class="form-group">
                    <label name="email">Your Email:</label>

                @if (Auth::check())
                    <input id="email" readonly="readonly" type='email' name="email" class="form-control" value="{{Auth::user()->email}}">
                @else
                    <input id="email"  type='email' name="email" class="form-control" value="">
                @endif

                </div>

                <div class="form-group">
                    <label name="subject">Subject:</label>
                    <input id="subject" name="subject" class="form-control">
                </div>

                <div class="form-group">
                    <label name="message">Message:</label>
                    <textarea id="message" name="message" class="form-control"></textarea>
                </div>
                </div>
                {{--@if(Auth::check())--}}
                <input type="submit" class="btn btn-success mb-5 mt-3" value ="Send Message">
                {{--@endif--}}


            </form>


        </div>
    </div>
@endsection