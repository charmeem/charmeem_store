@extends('app')
@section('title','| Story')
@section('story')
        <div class="row">
            <div class="col-md-12">
                <div class="jumbotron">
                    <h1 class="display-4">A real story</h1>
                    <p class="lead">Extract from divine book Quran, chapter Al-Araaf ( The Heights ):
                        </p>
                    <hr class="my-4">
                        <p class="story"> And We have certainly created you, [O Mankind], and given you [human] form. Then We said to the angels, "Prostrate to Adam"; so they prostrated, except for Iblees. He was not of those who prostrated.</p>
                    <hr class="mx-5">
                        <p>[Allah] said, "What prevented you from prostrating when I commanded you?" [Satan] said, "I am better than him. You created me from fire and created him from clay."</p>
                    <hr class="mx-5">
                        <p>[Allah] said, "Descend from Paradise, for it is not for you to be arrogant therein. So get out; indeed, you are of the debased.</p>
                    <hr class="mx-5">
                        <p>[Satan] said, "Reprieve me until the Day they are resurrected."</p>
                    <hr class="mx-5">
                        <p>[Allah] said, "Indeed, you are of those reprieved."</p>
                    <hr class="mx-5">
                        <p>[Satan] said, "Because You have put me in error, I will surely sit in wait for them on Your straight path.</p>
                    <hr class="mx-5">
                        <p>Then I will come to them from before them and from behind them and on their right and on their left, and You will not find most of them grateful [to You]."</p>
                    <hr class="mx-5">
                        <p>[Allah] said, "Get out of Paradise, reproached and expelled. Whoever follows you among them - I will surely fill Hell with you, all together."</p>
                    <hr class="mx-5">
                        <p>And "O Adam, dwell, you and your wife, in Paradise and eat from wherever you will but do not approach this tree, lest you be among the wrongdoers."</p>
                    <hr class="mx-5">
                        <p>But Satan whispered to them to make apparent to them that which was concealed from them of their private parts. He said, "Your Lord did not forbid you this tree except that you become angels or become of the immortal."</p>
                    <hr class="mx-5">
                        <p>And he swore [by Allah] to them, "Indeed, I am to you from among the sincere advisors."</p>
                    <hr class="mx-5">
                        <p>So he made them fall, through deception. And when they tasted of the tree, their private parts became apparent to them, and they began to fasten together over themselves from the leaves of Paradise. And their Lord called to them, "Did I not forbid you from that tree and tell you that Satan is to you a clear enemy?"</p>

                    <p class="lead">
                        <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
                    </p>
                </div>
            </div>
        </div>
@endsection