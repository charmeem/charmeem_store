<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
{{--@extends('layouts.app')--}}

{{--@section('content')--}}
<!-- Adding Sign In Modal -->
<div id="app"></div>
<div class="container" id="logincheckout">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <h5 align="center" class="mt-3 alert-dark">YOU NEED TO LOGIN OR REGISTER IN ORDER TO POST YOUR COMMENTS</h5>
                <div class="card-header">{{ __('Login') }}
                    <a href="{{route('aimeos_shop_list', ['site' => 'style'])}}" class="ml-5">Home</a>
                </div>

                @if ($errors->has('email'))
                    <div class="alert alert-warning">
                        <strong>{{ $errors->first('email') }}</strong>
                    </div>
                @endif

                @if(session()->has('error'))
                    <div class="alert alert-danger">
                        {{ session()->get('error') }}
                    </div>
                @endif

                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif

                <div class="card-body">
                    <!-- Redirect for vaildation to aimoeos_shop_login POST method login route-> checkoutloginController.php-> trait AthenticatesUser@login function -->
                    <form method="POST" action="{{ route('aimeos_shop_contactlog') }}" aria-label="{{ __('Login') }}">
                        @csrf

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!-- Adding customization for redirection to the previous page once login is done
                             Also see: loginController
                        -->
                    {{--@if ( Request::has('previous'))--}}
                    {{--<input type="hidden" name="previos" value="{{Request::get('previous')}}">--}}
                    {{--@else--}}
                    {{--<input type="hidden" name="previous" value="{{ URL::previous() }}">--}}
                    {{--@endif--}}
                    <!-- end charmeem -->

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-danger">
                                    {{ __('Login') }}
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    <small> {{ __('Forgot Password?') }} </small>
                                </a>
                            </div>
                            <div class=" checkout row justify-content-between mt-3 ">
                                <a class="ml-4 btn btn-info text-white" href="{{ route('aimeos_shop_contactreg') }}">
                                    {{ __('New ? Register here') }}
                                </a>

                                {{--<a class="mr-4 btn btn-primary" href="{{ route('aimeos_shop_checkout', ['site' => 'style', 'c_step?' =>'delivery']) }}">--}}
                                {{--{{ __('Or, Continue as Guest') }}--}}
                                {{--</a>--}}
                            </div>
                        </div>


                    </form>
                </div> <!-- card-body -->
            </div>    <!-- card -->
        </div>    <!-- col-md-8 -->
    </div>    <!-- row-->
</div>    <!-- container-->

</body>
</html>


{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--@endsection--}}
