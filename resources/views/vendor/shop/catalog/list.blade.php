
 <!--Custom view file adapted for CHARMEEM
    This view is rendered via controller CatalogController.php's
    method listAction mentioned in php artisan list:route
    The parameters of this list.blade file is obtained from Page.php
    as per listAction method Using shop/base layout which further make use of app layout
 -->

@extends('shop::base')
<!-- this is vendor/aimeos-laravel/views/base.blade.php file which further includes app.blade.php file which is our main template file -->

 @section('aimeos_header')
     <?= $aiheader['basket/mini'] ?>
     <?= $aiheader['catalog/filter'] ?>
     <?= $aiheader['catalog/stage'] ?>
     <?= $aiheader['catalog/lists'] ?>
     <?= $aiheader['catalog/tree'] ?>
     <?= $aiheader['catalog/carouselcat'] ?>
     <?= $aiheader['catalog/carouseltop'] ?>
 @stop

<!-- Mini basket -->
@section('aimeos_head')
    <?= $aibody['basket/mini'] ?>
@stop

<!-- Search bar and Category filter -->
 <!-- points to catalog/body-standard.php include Search input  and Tree ( catagory) feature -->
@section('aimeos_nav')
    <?= $aibody['catalog/filter'] ?>
@stop


@section('aimeos_stage')
    <?= $aibody['catalog/stage'] ?>
@stop

<!-- items list -->
    <!-- including carousels implementation here temporarily -->
@section('aimeos_body')
    <?= $aibody['catalog/carouseltop'] ?>
    <?= $aibody['catalog/carouselcat'] ?>
    <?= $aibody['catalog/lists'] ?>
    <!--  <= $aibody['catalog/tree'] ?>  -->

@stop
