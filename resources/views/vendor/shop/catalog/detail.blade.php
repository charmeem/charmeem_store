@extends('shop::base')

@section('aimeos_scripts')
    <!-- parents means the following scripts will be appended in parent 'shop::base' template script. -->
    @parent

    <!-- Adding thumbnail image zoom plugin -->
    <script type="text/javascript" src="{{asset('js/smoothproducts.min.js')}}"></script>

    <script>
        jQuery(window).load( function() {
            jQuery('.sp-wrap').smoothproducts();
        });
    </script>

@stop

@section('aimeos_header')
    <?= $aiheader['basket/mini'] ?>
    <?= $aiheader['catalog/filter'] ?>
    <?= $aiheader['catalog/stage'] ?>
    <?= $aiheader['catalog/detail'] ?>
    <?= $aiheader['catalog/session'] ?>
@stop

@section('aimeos_head')
    <?= $aibody['basket/mini'] ?>
@stop

<!-- Addded search bar  see also  shop.php -->
@section('aimeos_nav')
    <?= $aibody['catalog/filter'] ?>
@stop

@section('aimeos_stage')
    <?= $aibody['catalog/stage'] ?>
@stop

@section('aimeos_body')
    <?= $aibody['catalog/detail'] ?>
@stop

@section('aimeos_aside')
    <?= $aibody['catalog/session'] ?>
@stop
