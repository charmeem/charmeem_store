<footer class="footer">
    <div class="container">
        <div class="row ">
            <div class="col-md-6">
                <h5 class="mb-3">Charmeem Mall</h5>

                <div class="row mb-5">
                    <div class="col-4">
                        <ul class="list-unstyled">
                            <hr >
                            <li><a href="/shipping"><i class="fa fa-caret-right"></i>  Shipping Policies</a></li>
                            <hr >
                            <li><a href="/return"><i class="fa fa-caret-right"></i>  Return Policy</a></li>
                            <hr >

                        </ul>
                    </div>
                    <div class="col-4">
                        <ul class="list-unstyled">
                            {{--<li><a href=""><i class="fa fa-caret-right"></i>  New Arrival</a></li>--}}
                            <hr>
                            <li><a href="/sale"><i class="fa fa-caret-right"></i>  Sale &amp; Offers</a></li>
                            <hr >
                        </ul>
                    </div>
                </div>
                <ul class="nav">
                    <li class="nav-item"><a href="" class="nav-link pl-0"><i class="fa fa-facebook fa-lg"></i></a></li>
                    <li class="nav-item"><a href="" class="nav-link"><i class="fa fa-twitter fa-lg"></i></a></li>
                    {{--<li class="nav-item"><a href="" class="nav-link"><i class="fa fa-github fa-lg"></i></a></li>--}}
                    <li class="nav-item"><a href="" class="nav-link"><i class="fa fa-instagram fa-lg"></i></a></li>
                </ul>
                <br>
            </div>

            <div class="col-md-6">
                <h5 class="mb-3" >Join us</h5>

                <ul class="list-unstyled">
                    <li>

                        <a href="/checkoutlogin">
                            <div class="btn btn-info "><small>Login</small></div>
                        </a>
                        <span><small>Already Member?</small></span>
                    </li>
                    <hr class="mr-5">
                    <li>

                        <a href="/checkoutregister">
                            <div class="btn btn-dark "><small>Register</small></div>
                        </a>
                        <span><small>New?</small></span>
                    </li>
                    <hr class="mr-5">
                    <li>
                        <a href="/contact">
                            <div class="btn btn-secondary "><small>Contact Us</small></div>
                        </a>
                        <span><small>For complaint or suggestions</small></span>
                    </li>
                </ul>
            </div>
        </div>

        <div class="row justify-content-center mt-3">

            <p><small><i class="fa fa-copyright ">2018 CharmeemSoft@outlook.com. All rights reserved</i></small></p>

        </div>


        </div>
    </div>

</footer>