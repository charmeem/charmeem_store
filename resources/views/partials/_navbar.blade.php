{{--Nav bar--}}

<nav class="navbar navbar-default navbar-expand-sm">

    <!-- 3liner-hamburger setup icon in left part of header for small screens -->
    <!-- DONT REMOVE navbar-light OTHERWISE 3LINER ICON ILL NOT BE VISIBLE -->
    {{--<div class="mr-auto">--}}
    <button type="button" class="navbar-toggler navbar-dark" data-toggle="collapse" data-target="#collapse-1">
        {{--<span class="sr-only">Toggle Navigation</span>--}}
        <span class="navbar-toggler-icon"></span>
    </button>
{{--</div>--}}
<!-- Navigation bar -->
    <div class="collapse navbar-collapse mb-4"  id="collapse-1">

        <ul class=" navbar-nav ">
            <li class="nav-item"><a class="nav-link active" href="/">Home</a></li>
            {{--<li class="nav-item"><a class="nav-link" href="/story">A Story</a></li>--}}
            <li class="nav-item"><a class="nav-link" href="/faq">FAQ</a></li>
            {{--@if (Auth::check())--}}
                <li class="nav-item"><a class="nav-link" href="/contact">Contact us</a></li>
            {{--@else--}}
                <li class="nav-item"><a class="nav-link" href="/contactlog">Contact us</a></li>
        {{--@endif--}}

        <!-- Adding Modal for International shipping information -->
            <li class=" ship btn-sm " data-toggle="modal" data-target="#shipMod">Shipping</li>

        </ul>

        {{--Text on navbar--}}
        {{--<span class="shipping navbar-text ml-auto mr-2">--}}
        {{--Market you can trust on!--}}
        {{--<mark><em>Market you can trust on!</em></mark>--}}
        {{--Market <mark>you can trust on!</mark>--}}
        {{--<b>Market <mark>you can rely on!</mark></b>--}}
        {{--Only local Shipping--}}
        {{--</span>--}}



        <div id="shipMod" class="modal fade">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-body">
                        For International Shipping you can contact us below and get the latest shipping cost<br>

                    <!--<a href="{{url('/contact')}}">Click</a>-->
                    </div>
                </div>
            </div>
        </div>
    </div>

</nav>