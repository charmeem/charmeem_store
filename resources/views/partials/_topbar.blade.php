<div class="d-flex flex-wrap ">

    {{--Site brand name--}}
    <div class="ml-2">
        <a class="navbar-brand" href="#">
            <img src="{{asset('images/charmeem_mall_logo2.png')}}" alt="">
        </a>
    </div>

    {{-- Search bar,commenting out category in filter/body-standard.php--}}
    <div class="mt-1 ml-5 ml-auto">
        @yield('aimeos_nav')
    </div>

    <!-- Signin functionality on Home page-->
    <!-- Decided to remove it ,see lazy_laravel for detail -->


    <!-- Signin option display not on detail checkout page -->
    {{--@if (Request::url()==='http://charmeem/style/list')--}}
    {{--@if(!Request::is('*/detail/*'))--}}

<!--Checking if User already logged in-->
    <!--Plus implementing logout with dropdown when user name is clicked-->
    @if (Auth::check())
    <!--Modal Implementation for sign in  also see app.blade further signin implementation-->
        {{--<button class=" btn btn-sm btn-warning ml-sm-auto mr-sm-1" data-toggle="modal" data-target="#signinMod">--}}
        {{--<a href="#">--}}
        {{--<i class="fa fa-user"></i>--}}
        {{--<span class="navbar-text ml-auto"> Sign In</span>--}}
        {{--<b>Sign in</b>--}}
        {{--</a>--}}
        {{--</button>--}}

        {{--@else--}}

        <div class=" ml-auto mr-2 dropdown">
            <a id="navbarDropdown"  href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                <span class="navbar-text "> Hi, {{ Auth::user()->name }} </span>
                <i class="fa fa-user"></i>
            </a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </div>

@endif
{{--@endif--}}

<!-- mini-basket-->
    <div class="ml-auto">
        @yield('aimeos_head')
    </div>

</div><!--d-flex-->