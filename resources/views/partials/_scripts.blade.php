<!-- Scripts -->

<!-- Thumbnail image zoomin plugin 'smoothproducts' does not work well with jquery3..
 SO I TRIED TO ADD BOTH VERSIONS IN ORDER BELOW BUT-->
<!-- I have to live with only 2.1 at the moment as including both 2.1 and 3.2 causing
 problem with category slider -->

{{--Bootstrap 4--}}
<script type="text/javascript" src="{{asset('js/jquery-2.1.3.min.js')}}"></script>
{{--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>



@yield('aimeos_scripts')
<!-- js for Vue -->
<script src="{{asset('js/app.js')}}"></script>
{{--My own customized JS file--}}
<script src="{{asset('js/charmeem.js')}}"></script>


